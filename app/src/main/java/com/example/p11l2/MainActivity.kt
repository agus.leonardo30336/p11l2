package com.example.p11l2

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import androidx.appcompat.app.AlertDialog
import java.lang.Exception


class MainActivity : AppCompatActivity() {
    private lateinit var result: List<User>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        loadData()
        initListener()

    }

    private fun initListener() {
        search.setOnClickListener {
            var found = false
            for (i in result.indices) {
                if (et_name.text.toString() == result[i].name) {
                    showDialog(true, i)
                    found = true
                }
            }
            if (!found) showDialog(false, null)
        }
    }

    private fun loadData() {
        result = UserTransaction(this).viewAllName()
    }

    private fun showDialog(status: Boolean, i: Int?) {
        val statusDialog = AlertDialog.Builder(this)

        when (status) {
            true -> statusDialog.setTitle("Data Found")
                .setMessage(
                    "Nama : ${result[i!!].name}\n" +
                            "Email : ${result[i].email}\n" +
                            "No Telepon : ${result[i].no_hp}\n\n" +
                            "Apakah data sudah benar?"
                )
                .setPositiveButton("Edit") { dialog, which ->
                    val intent = Intent(this, EditActivity::class.java)
                    intent.putExtra("data", result[i])
                    startActivity(intent)
                }
                .setNegativeButton("Delete") { dialog, which ->
                    val args = arrayOf("${result[i].id}")
                    try {
                        contentResolver.delete(UserDB.MyContentProviderURI.CONTENT_URI, "id=?", args)
                        Toast.makeText(this, "Data dihapus", Toast.LENGTH_SHORT).show()
                    } catch (e: Exception) {
                        Toast.makeText(this, "error: $e", Toast.LENGTH_SHORT).show()
                    }

                }
                .setNeutralButton("Cancel") { dialog, which ->
                    dialog.dismiss()
                }

            false -> statusDialog.setTitle("Data Not Found")
                .setPositiveButton("OK") { dialog, which ->
                    dialog.dismiss()
                }

        }
        statusDialog.show()
    }

}
