package com.example.p11l2

import android.content.ContentValues
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_edit.*
import java.lang.Exception


class EditActivity : AppCompatActivity() {
    private lateinit var data: User
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        loadData()
        initListener()
    }

    private fun initListener() {
        btn_simpan.setOnClickListener {
            val args = arrayOf("${data.id}")
            val contentValues = ContentValues()
            contentValues.put(UserDB.UserTable.COLUMN_NAME, et_name.text.toString())
            contentValues.put(UserDB.UserTable.COLUMN_PHONE, et_nomor.text.toString())
            contentValues.put(UserDB.UserTable.COLUMN_EMAIL, et_email.text.toString())
            try {
                contentResolver.update(
                    UserDB.MyContentProviderURI.CONTENT_URI,
                    contentValues,
                    "id=?",
                    args
                )
                Toast.makeText(this, "Berhasil", Toast.LENGTH_SHORT).show()
            } catch (e: Exception) {
                Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show()
            }

        }
    }

    private fun loadData() {
        data = intent.extras!!.getParcelable("data")!!
        et_name.setText(data.name)
        et_email.setText(data.email)
        et_nomor.setText(data.no_hp)
    }
}