package com.example.p11l2

import android.net.Uri
import android.provider.BaseColumns

object UserDB {
    class UserTable: BaseColumns {
        companion object {
            val TABLE_USER = "user"
            val COLUMN_ID: String = "id"
            val COLUMN_NAME: String = "name"
            val COLUMN_EMAIL: String = "email"
            val COLUMN_PHONE: String = "phone"
        }
    }
    class MyContentProviderURI {
        companion object {
            private const val AUTHORITY = "com.example.p10l2.provider.MyContentProvider"
            private val USER_TABLE = UserDB.UserTable.TABLE_USER
            val CONTENT_URI: Uri = Uri.parse("content://$AUTHORITY/$USER_TABLE")
        }

    }
}

