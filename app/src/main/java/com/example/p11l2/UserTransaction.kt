package com.example.p11l2

import android.content.ContentResolver
import android.content.Context
import android.database.Cursor

class UserTransaction(context: Context) {
    private val myContentResolver: ContentResolver = context.contentResolver
    fun viewAllName(): List<User> {
        val data: ArrayList<User> = ArrayList()

        val cursor: Cursor? = myContentResolver.query(UserDB.MyContentProviderURI.CONTENT_URI, null, null, null, null)

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    data.add(
                        User(
                            cursor.getInt(cursor.getColumnIndex(UserDB.UserTable.COLUMN_ID)),
                            cursor.getString(cursor.getColumnIndex(UserDB.UserTable.COLUMN_NAME)),
                            cursor.getString(cursor.getColumnIndex(UserDB.UserTable.COLUMN_PHONE)),
                            cursor.getString(cursor.getColumnIndex(UserDB.UserTable.COLUMN_EMAIL))
                        )
                    )
                } while (cursor.moveToNext())
            }
            cursor.close()
        }
        return data
    }
}

