package com.example.p11l2


import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class User(
    val id: Int,
    val name: String,
    val no_hp: String,
    val email: String
) : Parcelable
